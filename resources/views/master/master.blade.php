<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 4/14/2016
 * Time: 8:07 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @yield('meta')

    <title>{{$title}}</title>

    @yield('header')

    <link rel="stylesheet" type="text/css" href="{{ elixir("css/all.css") }}">

    @yield('styles')

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=714438975253479";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

@yield('navbar')

<div id="main-content">
    @yield('content')
</div>

@include('master.footer')

<script src="{{ elixir("js/all.js") }}"></script>

@yield('scripts')


</body>
</html>
