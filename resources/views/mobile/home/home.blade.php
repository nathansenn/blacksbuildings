<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 10/19/2016
 * Time: 4:49 AM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <div class="mcontainer-fluid">
            <div class="menu">
                <h1 class="text-center">BARN ESTIMATOR</h1>
                <input id="btv" type="hidden">
            </div>


        <div class="mcontainer" id="f">
                <div class="text-center"><h4 >ESTIMATE</h4></div>
                <h3 text-center> <strong id="price">$5015.69</strong></h3>
                <p><em>*Tax installation, and freight are not included.</em></p>
                <p style="float: left"> Type:</p>
                <p id="bt">barn type</p>
                <p> Center:</p>
                <p style="float: left"> Size:</p>
                <p style="float: left" id="wt">20x</p>
                <p style="float: left" id="lt">30x</p>
                <p id="ht">10x</p>
                <p> Roll-Up Doors:</p>
                <p> Upgrades:</p>
        </div>

        <div style="width:100%; padding:15px; margin-top: 200px">
            <div class=" ">
                <h2> <strong>Step 1 | </strong> Building Type</h2>
            </div>

            <div class="open">
                <h2> OPEN BARN</h2>
                <div style="text-align: center">

                    <button onclick="ob()" type="button" class="btn btn-lg ">SELECT</button>

                </div>
            </div>
            <div class="openbody obdd" style="padding: 10px;">
                <div class="">
                    <label>Include Lumber? </label>
                    <input type="checkbox" name="checkbox" id="checkbox">
                </div>
                <div class="">
                    <form>
                        <label> Build Type: </label>
                        <label> Standard: </label>
                        <input type="radio" name="ob" id="obtype" value="Standard" checked>
                        <label> Economy: </label>
                        <input type="radio" name="ob" id="obtype" value="Economy">
                    </form>
                </div>
            </div>

            <div class="enclosed">
                <h2> ENCLOSED BARN</h2>
                <div style="text-align: center">
                    <button onclick="eb()" type="button" class="btn btn-lg ">SELECT</button>
                </div>
            </div>
            <div class="openbody ebdd" style="padding: 10px;">
                <div class="">
                    <label>Include Lumber? </label>
                    <input type="checkbox" name="checkbox" id="checkbox">
                </div>
                <div class="">
                    <form>
                        <label> Build Type: </label>
                        <label> Standard: </label>
                        <input type="radio" name="eb" id="ebtype" value="Standard" checked>
                        <label> Economy: </label>
                        <input type="radio" name="eb" id="ebtype" value="Economy">
                    </form>
                </div>
            </div>
            <!------ STEP 2 --------------------------------->


            <div class="step1">
                <div class="">
                    <h1> <strong>Step 2 | </strong> Building Size</h1>
                </div>
                <div class="openbody2" style="padding: 10px;">
                    <div class="">
                        <label> Center: </label>
                        <br>
                        <label style="float: left"> 10 Feet: </label>
                        <input type="radio" name="radio" id="ten">
                        <br>
                        <label style="float: left"> 12 Feet: </label>
                        <input type="radio" name="radio" id="twelve">
                        <br>

                        <!---- Width -->

                        <div class="list">
                            <label style="float: left; margin-top: 10px;" > Width: </label>
                            <select id="width" style="margin-top: 13px;">
                                <option value="20">20 Feet </option>
                                <option value="30">30 Feet </option>
                                <option value="40">40 Feet </option>
                            </select>
                        </div>
                        <br>

                        <div class="list">
                            <label style="float: left"> Length: </label>
                            <select id="length">
                                <option value="30">30 Feet </option>
                                <option value="40">40 Feet </option>
                            </select>
                        </div>
                        <br>

                        <div class="list">
                            <label style="float: left"> Height: </label>
                            <select id="height">
                                <option value="10">10 Feet </option>
                                <option value="12">12 Feet </option>
                                <option value="14">14 Feet </option>
                                <option value="16">16 Feet </option>

                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="note ">
                        <em> *Notes: For heights over 18 feet, please call us for an <br>estimate </em>
                    </div>
                </div>
            </div>
            <!-----  END OF STEP 2 ----->

            <!-----  STEP 3 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 3 | </strong> Lean To</h1>
                </div>

                <div class="openbody3">
                    <div class="width" style="padding: 15px">

                        <div class="list">
                            <label> Lean To: </label>
                            <select id="lts">
                                <option value="0">None</option>
                                <option value="1">One Side</option>
                                <option value="2">Two Sides</option>
                            </select>
                        </div>
                        <br>

                        <div class="list">
                            <label> Length: </label>
                            <select id="ltl">
                                <option value="10">10 Feet </option>
                                <option value="12">12 Feet </option>
                                <option value="14">12 Feet </option>
                            </select>
                        </div>
                        <br>

                        <div class="list">
                            <label> Width: </label>
                            <select id="ltw">
                                <option value="10">10 Feet </option>
                                <option value="20">20 Feet </option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="list" style="padding:15px;">

                        <label> Monitor </label> <input type="radio" name="radio" id="monitor">
                        <label> Transition </label><input type="radio" name="radio" id="Transition">
                        <label> Low Pitch </label><input type="radio" name="radio" id="LowPitch">

                    </div>

                    <div class="image">

                    </div>



                </div>
            </div>

            <!----- END OF STEP 3 ----->

            <!----- STEP 4 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 4 | </strong> Roll-Up Doors and Large Openings</h1>

                    <br>

                </div>


                <div class="openbody4">

                    <div class = "">
                        <br>

                    </div>
                    <div class="width">

                        <div class="list">
                            <form>
                                <label> Type:
                                    <select >
                                        <option>Opening Only</option>

                                    </select>
                                    <br>
                                </label>


                                <label> Location: </label>
                                <select>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>

                                </select>
                                <br>
                                <label> Width: </label>
                                <select>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>

                                </select>
                                <br>
                                <label> Height: </label>
                                <select>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>
                                    <option>12 Feet </option>

                                </select>

                            </form>
                        </div>






                        <br>

                        <br>

                    </div>




                    <div class="image">

                    </div>

                    <div class="note">
                        <em> *Notes: For heights over 18 feet, please call us for an <br>estimate </em>
                    </div>

                </div>
            </div>
            <!-----  END OF STEP 4 ----->



            <!----- STEP 5 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 5 | </strong> Entry Doors and Windows</h1>
                </div>
                <div class="openbody5">

                    <div class="list">
                        <label> Entry Doors*: </label>
                        <select>
                            <option>1</option>
                            <option>2 Feet </option>
                            <option>3 Feet </option>

                        </select>
                    </div>
                    <br>

                    <div class="list">
                        <label> Windows: </label>
                        <select>
                            <option>1</option>
                            <option>2 Feet </option>
                            <option>3 Feet </option>

                        </select>
                    </div>
                    <div class="image">

                    </div>

                    <div class="note">
                        <em> *Notes: For heights over 18 feet, please call us for an <br>estimate </em>
                    </div>

                </div>
            </div>
            <!----- END OF STEP 5 ----->

            <!----- STEP 6 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 6 | </strong> Upgrades</h1>
                </div>
                <div class="openbody6  ">


                    <div class="">
                        <div class="input-group">
                            <input type="checkbox"> <label> Roof Insulation</label>
                            <input type="checkbox"> <label> 26 Gauge Roof</label>


                        </div>
                        <!-- /input-group -->
                    </div>
                    <!-- /.col-lg-6 -->


                    <br>

                    <div class="list">
                        <label> Gable Top: </label>
                        <select>
                            <option>One Side</option>
                            <option>Two Sides</option>

                        </select>
                    </div>
                    <br>

                    <div class="list">
                        <label> Gable Ends: </label>
                        <select>
                            <option>One Side</option>
                            <option>Two Sides</option>


                        </select>
                    </div>

                    <br>

                    <div class="list">
                        <label> Sidewalls: </label>
                        <select>
                            <option>-none-</option>



                        </select>
                    </div>
                    <div class="image">

                    </div>



                </div>
            </div>

            <!----- END OF STEP 6 ----->


            <!----- STEP 7 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 7 | </strong> Shipping / Installation / Concrete</h1>
                </div>
                <div class="openbody7" style="padding: 15px">
                    <div class="">
                        <div class="input-group">
                            <img src="assets/images/information.png" alt="Placeholder image"  class="img-responsive">
                            <input type="checkbox"><label>Check for a <strong>SHIPPING</strong> estimate </label>

                        </div>
                        <p>The shipping estimate will usually be the maximum price that you could expect. If shopping costs ara a concern, please contact us and we will see if there are ways to
                            reduce this estimate(i.e. combining deliveries, changing drop-off location, etc..)</p>
                        <!-- /input-group -->

                    </div>

                    <div class="shipping">
                        <h3><strong>Shipping Address</strong> (Continental USA Only)* </h3>

                        <input type="text" class="form-control" placeholder="Enter a location">
                        <a href="#"><button type="button" class="btn btn-warning">Calculate Shipping Estimate</button><br></a>

                        <hr>

                        <input type="text" class="form-control" placeholder="Shipping Estimate: $--">


                    </div>



                    <div class="">
                        <hr>
                        <br>
                        <div class="input-group">
                            <img src="assets/images/information.png" alt="Placeholder image"  class="img-responsive">
                            <input type="checkbox"><label>Check for a <strong>INSTALLATION</strong> estimate </label>

                        </div>
                        <p>The shipping estimate will usually be the maximum price that you could expect. If shopping costs ara a concern, please contact us and we will see if there are ways to
                            reduce this estimate(i.e. combining deliveries, changing drop-off location, etc..)</p>
                        <!-- /input-group -->

                    </div>



                    <div class="">
                        <hr>
                        <br>
                        <div class="input-group">
                            <img src="assets/images/information.png" alt="Placeholder image"  class="img-responsive">
                            <input type="checkbox"><label>Check for a <strong>CONCRETE SLAB</strong> estimate </label>

                        </div>
                        <p>The shipping estimate will usually be the maximum price that you could expect. If shopping costs ara a concern, please contact us and we will see if there are ways to
                            reduce this estimate(i.e. combining deliveries, changing drop-off location, etc..)</p>
                        <!-- /input-group -->

                    </div>




                    <div class="">
                        <hr>
                        <br>
                        <div class="input-group">
                            <img src="assets/images/information.png" alt="Placeholder image"  class="img-responsive">
                            <input type="checkbox"><label>Check for a <strong>SALES TAX</strong> estimate </label>

                        </div>
                        <p>The shipping estimate will usually be the maximum price that you could expect. If shopping costs ara a concern, please contact us and we will see if there are ways to
                            reduce this estimate(i.e. combining deliveries, changing drop-off location, etc..)</p>
                        <!-- /input-group -->

                    </div>





                </div>



            </div>
            <!----- END OF STEP 7 ----->


            <!----- STEP 8 ----->
            <div class="step1">
                <div class="">
                    <h1> <strong>Step 8 | </strong> Customer Information</h1>
                </div>
                <div class="openbody8" style="padding: 15px">
                    <em> <strong> Note:</strong> You will be provided a link to a parts list for this barn when you submit this form. The partslist will also contain the shipping and installation estimate (if requested).</em>


                    <div >
                        <form>
                            <div class="list">
                                <br>

                                <label> Time Frame: </label>
                                <select>
                                    <option>Immediately</option>


                                </select>
                            </div>
                        </form>

                    </div>



                    <div class="image">

                    </div>

                    <form class="form-horizontal">
                        <div class="">
                            <label class="control-label" for="name">*Name:</label>
                            <div class="">
                                <input type="name" class="form-control" id="name">
                            </div>
                        </div>
                        <div class="">
                            <br>
                            <label class="control-label" for="email">*Email:</label>
                            <div class="">
                                <input type="email" class="form-control" id="email" >
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label" for="phone">*Phone:</label>
                            <div class="">
                                <input type="phone" class="form-control" id="phone">
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label" for="address">*Address</label>
                            <div class="">
                                <input type="address" class="form-control" id="address">
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label" for="city">*City</label>
                            <div class="">
                                <input type="city" class="form-control" id="city">
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label" for="state">*State</label>
                            <div class="">
                                <input type="state" class="form-control" id="state">
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label" for="zip">*Zip</label>
                            <div class="">
                                <input type="zip" class="form-control" id="zip">
                            </div>
                        </div>

                        <div class="">
                            <label class="control-label" for="zip">*Comments</label>
                            <div class="">
                                <textarea class="form-control" id="comment"></textarea>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <div class="checkbox">
                                    <label><input type="checkbox">Check if you would like for us to contact you. </label>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="button">
                                <a href="#"><button type="submit" class="submit btn-default">Submit</button></a>
                            </div>
                            <div class="button">
                                <a href="#"><button type="submit" class="btn btn-default">Reset</button> </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>


            <!----- END OF STEP 8----->

            <br>

            <!----- Disclaimer----->
            <div class="step1">
                <div class="disclaimer">
                    <p>Disclaimer: Estimates are not an official reviewed by BLACK'S METALS. Prices are subject to change due to
                        material price fluctuations. Please check local building codes before purchasing. Kit prices do not include
                        stamped engineered drawings but are available for an additional charge. We do not have truss drawings tha should satisfy the needs
                        of most building inspectors.</p>
                </div>
            </div>


            <!----- END of Disclaimer----->
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $('input:radio[name="ob"]').change(
                function(){
                    $('#bt').text($(this).val()+' Open Barn')
                });
        $('input:radio[name="eb"]').change(
                function(){
                    $('#bt').text($(this).val()+' Open Barn')
                });
        $('#width').change(
                function () {
                    cal();
                });
        $('#length').change(
                function () {
                    cal();
                });
        $('#height').change(
                function () {
                    cal();
                });
        function cal() {
            $('.step1').css('display','inline');
            $('.obdd').css('display','block');
            $('.ebdd').css('display','none');
            $('#bt').text($('#obtype').val()+ ' ' + $('#btv').val());
            $('#wt').text($( "#width" ).val());
            $('#lt').text($( "#length" ).val());
            $('#ht').text($( "#height" ).val());
            var price = $( "#length" ).val() * $( "#width" ).val() * 6;
            $('#price').text(price);
        }
        function ob() {
            $('#btv').val('Open Barn');
            cal();
        }
        function eb() {
            $('#btv').val('Enclosed Barn');
            cal();
        }
        var windw = this;

        $.fn.followTo = function ( pos ) {
            var $this = this,
                    $window = $(windw);

            $window.scroll(function(e){
                if ($window.scrollTop() < pos) {
                    $this.css({
                        position: 'absolute',
                        top: 'auto'
                    });
                } else {
                    $this.css({
                        position: 'fixed',
                        top: '0'
                    });
                }
            });
        };

        $('#f').followTo(250);

    </script>
@stop
