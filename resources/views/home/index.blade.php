<?php
/**
 * Created by PhpStorm.
 * User: nathan
 * Date: 10/19/2016
 * Time: 4:49 AM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')

    <!--header-->
    <header class="main-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="/" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="product">Package</a></li>
                                <li><a href="guide">Service</a></li>
                                <li><a href="gallery">Gallery</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s" >
                    <h3 class="title">BLACKS</h3>
                    <h3 class="title2">BUILDINGS</h3>
                    <h4 class="sub-title">At your service.</h4>
                    <button type="submit" class="btn btn-submit">Take a peek</button>
                </div>
            </div>
        </div>
    </header>
    <!--/ header-->


    <section id="about">
        <div class="container">
            <div class="col-lg-12">
                <h1>WELCOME TO BLACK'S BUILDINGS</h1>

                <h2> Welcome to Black's Buildings </h2>

                <p><strong>This is a list of house types.</strong> Houses can be built in a large variety of configurations. A basic division is between
                    <br>free-standing or Single-family houses and various types of attached or multi-user dwellings. Both may vary greatly in scale and amount
                    <br> of accommodation provided. Although there appear to be many different types, many of the variations listed below are purely
                    <br> matters of style rather than spatial arrangement or scale. Some of the terms listed are only used in some parts of the English-speaking
                    world.
                </p>

                <div class="fb-page" data-href="https://www.facebook.com/blacksbuildingstn/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
                    <blockquote cite="https://www.facebook.com/blacksbuildingstn/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/blacksbuildingstn/">Blacks Buildings</a>
                    </blockquote>
                </div>
                <br>
                <br>
                <a href="about"><button type="submit" class="btn btn-info">Learn More</button></a>


            </div>

        </div>
    </section>

    <!---->
    <section id="package-section" class="section-padding wow fadeInUp delay-05s">
         <div class="t3" id="container" >
            <div class="margin50">
             <div class="col-lg-6 opt1">
                 <div class="options">
                     <h1> 3 TYPES</h1>
                    <ul>
                        <li><a href="product"><img src="assets/images/metal.png">Metal <br> Buildings</a></li>
                        <li><a href="product"><img src="assets/images/stainedwood.png">Stained Wood <br>Buildings</a></li>
                        <li><a href="product"><img src="assets/images/painted.png">Painted Wood<br> Buildings</a></li>
                    </ul>

                     <br>

                 </div>

             <div class="options2">
                 <h1> BUILDING OPTIONS</h1>
                 <label>Windows:</label>
                 <div class="pull-right">
                           <select>
                           <a href="product"><option selected>2'x3' Single Pane Window- $90</option></a>
                            <a href="product"><option>2'x3' Double Pane Window- $140</option></a>
                            <a href="product"><option>3'x3' Single Pane Window- $110</option></a>
                        </select>
                  </div>


                 <br>
                 <label>Shelving:</label>
                 <div class="pull-right">
                         <select>
                             <a href="product"><option selected>4' Loft- $50</option></a>
                             <a href="product"><option>8' Loft- $75</option></a>
                             <a href="product"><option>3-Tier Shelves- $12/ foot</option></a>
                             <a href="product"><option>Workbench- $10/ foot</option></a>
                         </select>
                  </div>

                 <br>
                 <label>Doors:</label>
                 <div class="pull-right">
                         <select>
                             <a href="product"><option selected>3' Entry Door- $150</option></a>
                             <a href="product"><option>6'x7' Roll-Up Door-$350</option></a>
                             <a href="product"><option>8'x7' Roll-Up Door- $450</option></a>
                             <a href="product"><option>9'x7' Roll-Up Door- $500</option></a>
                         </select>
                 </div>
                 <br>
                 <label>Flooring Updates:</label>
                 <div class="pull-right">
                         <select>
                             <a href="product"><option selected>3/4" Plywood- $.30/ square foot</option></a>
                             <a href="product"><option>3/4" Advantech- $.30/ square foot</option></a>
                             <a href="product"><option>12" On Center Floor Joists-$.30 square foot</option></a>
                             <a href="product"><option>2"x6" Floor Joists- $.30 square foot</option></a>
                         </select>
                 </div>
                 <br>
                 <label>Miscellaneous Options: </label>
                 <div class="pull-right">
                        <select>
                            <a href="product"> <option selected>Ridgelight Skylight- $50 per 10' section</option></a>
                            <a href="product"> <option>Hole For A/C Unit- $50</option></a>
                            <a href="product"> <option>Single Slope Roof- Additional $200-$300</option></a>
                         </select>


                </div>
                 <br>
                 <br>


                 </div>

             </div>
                <br>
                <br>

             <div class="col-md-4">
                 <label> STANDARD SIZES </label>

                 <table class="table mainTable">
                     <thead>
                     <tr>
                         <th>10"WIDE</th>
                         <th>12"WIDE</th>
                         <th>14"WIDE</th>
                     </tr>
                     </thead>
                     <tbody>
                     <tr>
                         <td>10x12</td>
                         <td>12x16</td>
                         <td>14x20</td>

                     </tr>
                     <tr>
                         <td>10x16</td>
                         <td>12x20</td>
                         <td>14x24</td>

                     </tr>
                     <tr>
                         <td>10x24</td>
                         <td>12x24</td>
                         <td>14x28</td>
                     </tr>
                     <tr>
                         <td></td>
                         <td>12x28</td>
                         <td>14x32</td>

                     </tr>
                     <tr>
                         <td></td>
                         <td>12x32</td>
                         <td></td>

                     </tr>
                     </tbody>
                 </table>

             </div>

            </div>
         </div>

    </section>
    <!--TEAM SECTION-->

    <!--END OF TEAM SECTION-->

    <!-- SERVICE SECTION---->
    <section class="section-padding wow fadeInUp delay-02s" id="service-section">
        <div class="container">
            <div class ="col-md-12 col-sm-12">

                <a href="guide"><img src="assets/images/buy.png" alt="Placeholder image" width="100" height="100" class="img-responsive" hspace="20"></a>

                <div class = "col-md-8 col-sm-8 service-content">

                    <h2>BUY OR RENT TO OWN </h2>
                    <p>Our convenient purchasing options allow you to pay for your building at the time of purchase or to make monthly payments through our rent to
                        <br>own program. Our low monthly rental rates are comparable to mini-warehousing rates. Our no-strings plan allows you to have your own
                        storage facility at your home or business, when and where you need it.
                    </p>

                    <p><strong>No Credit Checks:</strong>Just a small security deposit plus your first month’s rent and we’ll set up a building at your location. </p>
                    <p><strong>No Strings:</strong>The rental agreement is on a month to month basis, meaning you have the flexibility to turn in the building at anytime
                        for  <br> any reason.
                    </p>

                    <p><strong>Early Payoff: </strong>Payoff anytime with no penalty. There is also a discount off the rental balance
                        for those who pay off early. See dealer for details. </p>
                </div>

            </div>
            <br>

            <div class="col-md-12 col-sm-12">
                <a href="guide"><img src="assets/images/pricing.png" alt="Placeholder image" width="100" height="100" class="img-responsive"></a>

                <div class = "col-md-8 col-sm-8 service-content">

                    <h2>PRICING</h2>

                    <p>For pricing, visit your local  dealer or click here and we’ll send you a product brochure.
                        If you find the same quality building elsewhere for a
                        <br> lower price, we'll match it every time. With our Low Price Guarantee, you can rest easy knowing that we won’t be undersold.We strive to
                    </p>
                    <p>produce the highest quality buildings at the most reasonable prices giving you maximum value every time. </p>
                </div>

            </div>

            <br>

            <div class="col-md-12 col-sm-12 service">
                <a href="guide"><img src="assets/images/quick.png" alt="Placeholder image" width="100" height="100" class="img-responsive"></a>

                <div class = "col-md-8 col-sm-8 service-content">
                    <h2>QUICK FREE DELIVERY &amp; EASY SETUP </h2>
                    <p>Buildings at the sales lot can usually be delivered within 5 week days (weather permitting). </p>
                    <p>Ordered Treated & Fir buildings can usually be delivered within 10 to 15 days (weather permitting). </p>
                    <p>Ordered Painted and Metal buildings can usually be delivered within 15 to 20 days (weather permitting). </p>
                    <p>Note: Non-standard metal colors on any building will add one week to the lead time. </p>
                    <p>No site preparation necessary (if site is accessible with truck and trailer and site is no more than 3 feet out of level). </p>
                    <p>Free setup includes leveling with customer supplied concrete blocks and driver supplied pressure treated shims. Drivers can supply concrete
                        <br> blocks for a minimal charge. </p>
                    <p>First 30 miles free, over 30 miles subject to additional charge. </p>
                </div>

            </div>



        </div>
    </section>
    <!--END OF SERVICE SECTION---->

    <!--CONTACT SECTION-->
    <section class="section-padding wow fadeInUp delay-05s" id="contact">
        <div class="container">
            <div class="row white">
                <div class="col-md-8 col-sm-12">
                    <div class="section-title">
                        <h2 class="head-title black">Contact Us</h2>
                        <hr class="botm-line">
                        <p class="sec-para black">How can we help you? Please select one of the topics below so we can direct you to the right customer service
                            resource.</p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-6" style="padding-left:0px;">
                        <h3 class="cont-title">Email Us</h3>
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>
                        <div class="contact-info">
                            <form action="" method="post" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="fname" class="form-control" id="fname" placeholder="Firstname" data-rule="minlen:4" data-msg="Please
                                    	enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="lname" class="form-control" id="lname" placeholder="Lastname" data-rule="minlen:4" data-msg="Please
                                    	enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>


                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please
                                    	enter a valid email" />
                                    <div class="validation"></div>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Telephone No." data-rule="minlen:4" data-msg=
                                    "Please enter a valid number" />
                                    <div class="validation"></div>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us"
                                              placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <a href="/"></a><button type="submit" class="btn btn-send">Send</button></a>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h3 class="cont-title">Visit Us</h3>
                        <div class="location-info">
                            <p class="white"><span aria-hidden="true" class="fa fa-map-marker"></span>615 Briskin Ln Lebanon, Tennesse</p>

                            <p class="white"><span aria-hidden="true" class="fa fa-phone"></span>Phone:+1 615-587-0923</p>
                            <p class="white"><span aria-hidden="true" class="fa fa-envelope"></span>Email: <a href="" class="link-dec">blacks@gmail.com</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-icon-container hidden-md hidden-sm hidden-xs">
                            <span aria-hidden="true" class="fa fa-mobile"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END OF CONTACT SECTION-->

    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->
@stop

@section('scripts')

@stop