<?php
/**
 * Created by PhpStorm.
 * User: Carbon
 * Date: 11/13/2016
 * Time: 4:49 PM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <!--header-->
    <header class="about-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="/" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="product">Package</a></li>
                                <li class="active"><a href="guide">Service</a></li>
                                <li><a href="gallery">Gallery</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s">
                    <h3 class="title3">BUYING GUIDE</h3>


                </div>
            </div>
        </div>
    </header>
    <!--/ header-->

    <!-- SERVICE SECTION---->
    <section class="section-padding wow fadeInUp delay-02s" id="guide-section">
        <div class="container">
            <div class ="col-md-12 col-sm-12 col-lg-10 col-lg-offset-2">

                <a href=""></a><img src="assets/images/buy.png" alt="Placeholder image" width="100" height="100" class="img-responsive" hspace="20"></a>

                <div class = "col-md-8 col-sm-8 service-content">

                    <h2>BUY OR RENT TO OWN </h2>
                    <p>Our convenient purchasing options allow you to pay for your building at the time of purchase or to make monthly payments through our rent to
                        <br>own program. Our low monthly rental rates are comparable to mini-warehousing rates. Our no-strings plan allows you to have your own
                        storage facility at your home or business, when and where you need it.
                    </p>

                    <p><strong>No Credit Checks:</strong>Just a small security deposit plus your first month’s rent and we’ll set up a building at your location. </p>
                    <p><strong>No Strings:</strong>The rental agreement is on a month to month basis, meaning you have the flexibility to turn in the building at anytime
                        for  <br> any reason.
                    </p>

                    <p><strong>Early Payoff: </strong>Payoff anytime with no penalty. There is also a discount off the rental balance
                        for those who pay off early. See dealer for details. </p>
                </div>

            </div>
            <br>

            <div class="col-md-12 col-sm-12 col-lg-10 col-lg-offset-2">
                <img src="assets/images/pricing.png" alt="Placeholder image" width="100" height="100" class="img-responsive">

                <div class = "col-md-8 col-sm-8 service-content">

                    <h2>PRICING</h2>

                    <p>For pricing, visit your local  dealer or click here and we’ll send you a product brochure.
                        If you find the same quality building elsewhere for a
                        <br> lower price, we'll match it every time. With our Low Price Guarantee, you can rest easy knowing that we won’t be undersold.We strive to
                    </p>
                    <p>produce the highest quality buildings at the most reasonable prices giving you maximum value every time. </p>
                </div>

            </div>

            <br>

            <div class="col-md-12 col-sm-12 col-lg-10 col-lg-offset-2">
                <img src="assets/images/quick.png" alt="Placeholder image" width="100" height="100" class="img-responsive">

                <div class = "col-md-8 col-sm-8 service-content">
                    <h2>QUICK FREE DELIVERY &amp; EASY SETUP </h2>
                    <p>Buildings at the sales lot can usually be delivered within 5 week days (weather permitting). </p>
                    <p>Ordered Treated & Fir buildings can usually be delivered within 10 to 15 days (weather permitting). </p>
                    <p>Ordered Painted and Metal buildings can usually be delivered within 15 to 20 days (weather permitting). </p>
                    <p>Note: Non-standard metal colors on any building will add one week to the lead time. </p>
                    <p>No site preparation necessary (if site is accessible with truck and trailer and site is no more than 3 feet out of level). </p>
                    <p>Free setup includes leveling with customer supplied concrete blocks and driver supplied pressure treated shims. Drivers can supply concrete
                        <br> blocks for a minimal charge. </p>
                    <p>First 30 miles free, over 30 miles subject to additional charge. </p>
                </div>

            </div>



        </div>
    </section>






    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->
@stop

@section('scripts')

@stop