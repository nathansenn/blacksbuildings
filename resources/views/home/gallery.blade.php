<?php
/**
 * Created by PhpStorm.
 * User: Carbon
 * Date: 11/13/2016
 * Time: 4:49 PM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <!--header-->
    <header class="about-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="/" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="product">Package</a></li>
                                <li><a href="guide">Service</a></li>
                                <li class="active"><a href="gallery">Gallery</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s">
                    <h3 class="title3">GALLERY</h3>
                </div>
            </div>
        </div>
    </header>
    <!--/ header-->

    <!-- start portfolio -->
    <br>
    <section id="gallery">
        <div class="container">
            <h3  class="wow bounce">DISCOVER WORKS OF ART </h3>
            <p>Millipedes are a class (Diplopoda) of arthropods, characterised by two pairs of jointed legs on most body segments. Most species have
                long cylindrical or flattened bodies with more than 20 segments, while pill millipedes are shorter and can roll into a ball.  </p>


            <div class="row">
                <br>

                <br>

                <div class="col-md-12 text-center">


                    <div class="gal-button">
                        <button class="btn btn-primary filter-button" data-filter="all">All</button>
                        <button class="btn btn-default filter-button" data-filter="barn">Barn 1</button>
                        <button class="btn btn-default filter-button" data-filter="barn2">Barn 2</button>
                        <button class="btn btn-default filter-button" data-filter="barn3">Barn 3</button>
                    </div>
                    <br/>



                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn ">
                        <div class="gallery-thumb">


                            <img class="port-image fancybox" src="assets/images/type1-1.png" alt="Gallery img" class="fluid-img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 1</h4>

                            </div>

                        </div>

                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type1-1.png" alt="Gallery img" class="fluid-img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 1</h4>

                            </div>

                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>
                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>

                        </div>
                    </div>

                    <div class="col-md-3 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div clas="pull-right">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>
        </div>
    </section>





    <br>
    <!-- end portfolio -->






    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->
@stop

@section('scripts')

@stop