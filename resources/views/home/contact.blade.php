<?php
/**
 * Created by PhpStorm.
 * User: Carbon
 * Date: 11/13/2016
 * Time: 4:49 PM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <!--header-->
    <header class="about-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="/" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="product">Package</a></li>
                                <li><a href="guide">Service</a></li>
                                <li><a href="gallery">Gallery</a></li>
                                <li class="active"><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s">
                    <h3 class="title3">CONTACT US</h3>


                </div>
            </div>
        </div>
    </header>
    <!--/ header-->


    <!--CONTACT SECTION-->
    <section class="section-padding wow fadeInUp delay-05s" id="contact">
        <div class="container">
            <div class="row white">
                <div class="col-md-8 col-sm-12">
                    <div class="section-title">
                        <h2 class="head-title black">Contact Us</h2>
                        <hr class="botm-line">
                        <p class="sec-para black">How can we help you? Please select one of the topics below so we can direct you to the right customer service
                            resource.</p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-6" style="padding-left:0px;">
                        <h3 class="cont-title">Email Us</h3>
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>
                        <div class="contact-info">
                            <form action="" method="post" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="fname" class="form-control" id="fname" placeholder="Firstname" data-rule="minlen:4" data-msg="Please 
                                    	enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="lname" class="form-control" id="lname" placeholder="Lastname" data-rule="minlen:4" data-msg="Please 
                                    	enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>


                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please 
                                    	enter a valid email" />
                                    <div class="validation"></div>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Telephone No." data-rule="minlen:4" data-msg=
                                    "Please enter a valid number" />
                                    <div class="validation"></div>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us"
                                              placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <a href="/"><button type="submit" class="btn btn-send">Send</button></a>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h3 class="cont-title">Visit Us</h3>
                        <div class="location-info">
                            <p class="white"><span aria-hidden="true" class="fa fa-map-marker"></span>Matina Davao City, Philippines</p>
                            <p class="white"><span aria-hidden="true" class="fa fa-phone"></span>Phone:1234567890</p>
                            <p class="white"><span aria-hidden="true" class="fa fa-envelope"></span>Email: <a href="" class="link-dec">blacks@gmail.com</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-icon-container hidden-md hidden-sm hidden-xs">
                            <span aria-hidden="true" class="fa fa-mobile"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END OF CONTACT SECTION-->


    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->


@stop

@section('scripts')

@stop