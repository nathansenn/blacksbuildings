<?php
/**
 * Created by PhpStorm.
 * User: Carbon
 * Date: 11/13/2016
 * Time: 4:49 PM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <!--header-->
    <header class="about-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="/" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li class="active"><a href="about">About</a></li>
                                <li><a href="product">Package</a></li>
                                <li><a href="guide">Service</a></li>
                                <li><a href="gallery">Gallery</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s">
                    <h3 class="title3">ABOUT US</h3>
                </div>
            </div>
        </div>
    </header>
    <!--/ header-->

    <!-- WHO WE ARE SECTION-->
    <section id="about">
        <div id="container">
            <div class="col-lg-6 who">

                <h2>WHO WE ARE</h2>

                <p>Our convenient purchasing options allow you to pay for your building at the time of purchase or to make monthly
                    payments through our rent to own program. Our low monthly rental rates are comparable to mini-warehousing rates.
                    Our no-strings plan allows you to have your own storage facility at your home or business, when and where you need it.
                </p>

                <p>Our convenient purchasing options allow you to pay for your building at the time of purchase
                    or to make monthly payments through our rent to own program. Our low monthly rental rates
                    are comparable to mini-warehousing rates. </p>

                <img src="assets/images/who1-1.jpg" alt="Who1-1" width="700" height="400" class="img-responsive">
                <em>*Our convenient purchasing options allow to pay for your building. </em>


            </div>

            <div class="col-lg-6 who1 pull-right">

                <div class="col-lg-12 trust">

                    <h2>A COMPANY YOU CAN TRUST </h2>
                    <img src="assets/images/trust.jpg" alt="Placeholder image" height="68" class="img-circle img-responsive">


                    <p>Our convenient purchasing options allow you to pay for your building at
                        the time of purchase or to make monthly payments through
                        our rent to own program. Our low monthly rental rates are comparable to mini-warehousing rates.
                    </p>
                    <br>

                </div>
                <br>

                <div class="col-lg-12 craft">
                    <h2>A TRADITION OF CRAFTMANSHIP</h2>
                    <img src="assets/images/craft.jpg" alt="Placeholder image" height="68" class="img-circle img-responsive">


                    <p>Our convenient purchasing options allow you to pay for your building
                        at the time of purchase or to make monthly payments through our rent
                        to own program. Our low monthly rental rates are comparable to mini-warehousing rates.
                    </p>
                    <br>

                </div>

                <br>

                <div class="col-lg-12 qua-service">

                    <img src="assets/images/qua-service.jpg" alt="Placeholder image" height="68" class="img-circle img-responsive">

                    <h2> QUALITY &amp; SERVICE </h2>

                    <p> Our convenient purchasing options allow you to pay for your
                        building at the time of purchase or to make monthly payments
                        through our rent to own program. Our low monthly
                        rental rates are comparable to mini-warehousing rates.
                    </p>
                    <br>

                </div>

            </div>
        </div>


    </section>
    <!-- END OF WHO WE ARE SECTION-->


    <section id="content2">
        <div class="container">
            <div class="col-lg-12">
                <h1>WELCOME TO BLACK'S BUILDINGS</h1>

                <h2> Welcome to Black's Buildings </h2>

                <p><strong>This is a list of house types.</strong> Houses can be built in a large variety of configurations. A basic division is between
                    <br>free-standing or Single-family houses and various types of attached or multi-user dwellings. Both may vary greatly in scale and amount
                    <br> of accommodation provided. Although there appear to be many different types, many of the variations listed below are purely
                    <br> matters of style rather than spatial arrangement or scale. Some of the terms listed are only used in some parts of the English-speaking
                    world.
                </p>
                <br>
            </div>

        </div>
    </section>

    <!-- start portfolio -->
    <section id="gallery">
        <div class="container">

            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="wow bounce">GALLERIES</h2>
                    <div class="gal-button">
                        <button class="btn btn-primary filter-button" data-filter="all">All</button>
                        <button class="btn btn-default filter-button" data-filter="barn">Barn 1</button>
                        <button class="btn btn-default filter-button" data-filter="barn2">Barn 2</button>
                        <button class="btn btn-default filter-button" data-filter="barn3">Barn 3</button>
                    </div>
                    <br/>



                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn ">
                        <div class="gallery-thumb">


                            <img class="port-image fancybox" src="assets/images/type1-1.png" alt="Gallery img" class="fluid-img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 1</h4>

                            </div>

                        </div>

                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type1-1.png" alt="Gallery img" class="fluid-img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 1</h4>

                            </div>

                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn2">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type2-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 2</h4>

                            </div>
                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>
                        </div>
                    </div>

                    <div class="iso-box col-md-3 col-sm-6 col-xs-12 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>

                        </div>
                    </div>

                    <div class="col-md-3 filter barn3 ">
                        <div class="gallery-thumb">
                            <img class="port-image" src="assets/images/type3-1.png" class="fluid-img" alt="Gallery img"/>
                            <div class="gallery-overlay">
                                <a href="#" class="fa fa-search"></a>
                                <a href="#" class="fa fa-link"></a>
                                <h4>BARN 3</h4>

                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>

        <div class="btn1">
            <a href="product"><button type="submit" class="btn btn-info">View Package</button></a>
        </div>

    </section>





    <br>
    <!-- end portfolio -->






    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->

@stop

@section('scripts')

@stop