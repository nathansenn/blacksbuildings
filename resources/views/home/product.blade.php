<?php
/**
 * Created by PhpStorm.
 * User: Carbon
 * Date: 11/13/2016
 * Time: 4:49 PM
 */
?>
@extends('master.master')

@section('header')

@stop

@section('navbar')

@stop

@section('content')
    <!--header-->
    <header class="about-header" id="header">
        <div class="bg-color">
            <!--nav-->
            <nav class="nav navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                                <span class="fa fa-bars"></span>
                            </button>
                            <a href="index.html" class="navbar-brand"><img src="assets/images/logo.png" alt="logo" width="120" height="100" class="img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="mynavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li class="active"><a href="product">Package</a></li>
                                <li><a href="guide">Service</a></li>
                                <li><a href="gallery">Gallery</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/ nav-->
            <div class="container text-center">
                <div class="wrapper wow fadeInUp delay-05s">
                    <h3 class="title3">OUR PRODUCT</h3>


                </div>
            </div>
        </div>
    </header>
    <!--/ header-->


    <!--PRODUCT SECTION-->
    <section class="section-padding wow fadeInUp delay-05s" id="prod">
        <div class="container">


            <!-- <div class="row estimate">
                <h4 class="text-center">ESTIMATE</h4>
                <br>
                <h3 class="text-center">$5015.69</h3>
                <em>*Tax installation, and freight are not included.</em>
                <hr >
                <label> Type:</label>
                <br>
                <label> Size:</label>
                <br>
                <label> Add-on Packages:</label>
                <br>
                <label> Roofing Options:</label>
                <br>
                <label> Siding &amp; Trim:</label>
                <br>
                <br>

            </div> -->


            <div class="steps">

                <div class="col-sm-12">
                    <br>
                    <h2> <strong>STEP 1 | </strong> Building Type</h2>
                </div>
                <br>

                <div class="col-sm-5 type1">
                    <h2> METAL BUILDINGS</h2>
                    <a href= "#"> <button type="button" class="btn btn-lg">SELECT</button></a>
                    <br>


                </div>
                <div class="col-sm-5 type2">
                    <h2>STAINED WOOD BUILDINGS</h2>
                    <a href= "#"> <button type="button" class="btn btn-lg">SELECT</button></a>
                    <br>


                </div>
                <div class="col-sm-5 type3">
                    <h2>PAINTED WOOD BUILDINGS</h2>
                    <a href= "#"> <button type="button" class="btn btn-lg">SELECT</button></a>
                    <br>
                </div>

            </div>




            <!--- STEP2 --->
            <div class="steps">
                <div class="col-sm-6">
                    <br>
                    <h2> <strong>STEP 2 | </strong> Windows</h2>
                </div>
                <br>

                <div class="col-sm-5 step2">
                    <ul>
                        <li><a href=""><img src="assets/images/single.png"><small>2'x3' <br>
                                    Single Pane Window- <br> <strong>$90</strong></small></a></li>
                        <li><a href=""><img src="assets/images/doublepane.png"><small>2'x3'  <br>Double Pane Window-<br><strong>$140</strong></small></a></li>
                        <li><a href=""><img src="assets/images/single.png"><small>3'x3'  <br>Single Pane Window- <br><strong> $90</strong></small></a></li>
                    </ul>

                </div>

                <!---END OF  STEP2 --->

                <!---STEP3 --->
            </div>
            <div class="steps">
                <div class="col-sm-6"> <br>
                    <h2> <strong>STEP 3 | </strong> Shelving</h2>
                </div>
                <br>
                <div class="col-sm-5 step2">
                    <ul class="boptions">
                        <li><a href=""><img src="assets/images/loft.png"><small>4' Loft-  <br> <strong>$50</strong></small></a></li>
                        <li><a href=""><img src="assets/images/loft.png"><small>8' ' Loft-  <br><strong>$75</strong></small></a></li>
                        <li><a href=""><img src="assets/images/shelves.png"><small>Tier Shelves<br><strong> $12/ foot</strong></small></a></li>
                        <li><a href=""><img src="assets/images/workbench.png"><small>Tier Shelves<br><strong> $12/ foot</strong></small></a></li>
                    </ul>

                </div>
                <!---END OF STEP3 --->

                <!---STEP4 --->
            </div>
            <div class="steps">
                <div class="col-sm-6"> <br>
                    <h2> <strong>STEP 4 | </strong> Doors</h2>
                </div>
                <br>
                <div class="col-sm-5 step2">
                    <ul class="boptions">
                        <li><a href=""><img src="assets/images/doublepane.png"><small>3' Entry Door-  <br> <strong>$150</strong></small></a></li>
                        <li><a href=""><img src="assets/images/metal.png"><small>6'x7' Roll-Up Door- <br><strong>$350</strong></small></a></li>
                        <li><a href=""><img src="assets/images/metal.png"><small>8'x7' Roll-Up Door- <br><strong>$450</strong></small></a></li>
                        <li><a href=""><img src="assets/images/metal.png"><small>9'x7' Roll-Up Door-<br><strong> $500</strong></small></a></li>
                    </ul>
                </div>
                <!---END OF STEP4 --->
                <!---STEP5 --->


            </div>
            <div class="steps">
                <div class="col-sm-6"> <br>
                    <h2> <strong>STEP 5 | </strong>Flooring Upgrades</h2>
                </div>
                <br>
                <div class="col-sm-5 step2">

                    <ul class="boptions">
                        <li><a href=""><img src="assets/images/plywood.png"><small>3/4" Plywood-  <br> <strong>$.30/ square foot</strong></small></a></li>
                        <li><a href=""><img src="assets/images/advantech.png"><small>3/4" Advantech- <br><strong>$.30/ square foot</strong></small></a></li>
                        <li><a href=""><img src="assets/images/joist.png"><small>12" On Center "<br>Floor Joists<br><strong>$.30/ square foot</strong></small></a></li>
                        <li><a href=""><img src="assets/images/floorjoist.png"><small>2"x6" Floor Joists<br><strong>$.30/ square foot</strong></small></a></li>
                    </ul>
                </div>

                <!---END OF STEP5 --->
                <!---STEP6 --->

            </div>
            <div class="steps">
                <div class="col-sm-6"> <br>
                    <h2> <strong>STEP 6 | </strong>Miscellaneous Options</h2>
                </div>
                <br>
                <div class="col-sm-5 step2">
                    <ul>
                        <li><a href=""><img src="assets/images/ridgelight.png"><small>Ridgelight Skylight-  <br> <strong>$50 per 10' sec</strong></small></a></li>
                        <li><a href=""><img src="assets/images/ac.png"><small> Hole For A/C Unit-<br><strong> $50</strong></small></a></li>
                        <li><a href=""><img src="assets/images/singleslope.png"><small> Single Slope Roof-<br><strong>Add'l $200-$300</strong></small></a></li>
                    </ul>

                </div>
                <!---END OF STEP6 --->
            </div>
            <br>
            <div class="btn1">
                <a href="gallery"><button type="submit" class="btn btn-info">View Gallery</button></a>
            </div>
    </section>
    <!--END OF PRODUCT  SECTION-->


    <!--FOOTER SECTION-->
    <footer class="" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 footer-copyright">
                    © Blacks Building- All rights reserved

                </div>
                <div class="col-sm-5 footer-social">
                    <div class="pull-right hidden-xs hidden-sm">
                        <a href="https://www.facebook.com/blacksbuildingstn/"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--END OF FOOTER SECTION-->
@stop

@section('scripts')

@stop