<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'home@index'
]);
Route::get('/about', [
    'as' => 'about',
    'uses' => 'home@about'
]);

Route::get('/contact', [
    'as' => 'contact',
    'uses' => 'home@contact'
]);
Route::get('/gallery', [
    'as' => 'gallery',
    'uses' => 'home@gallery'
]);
Route::get('/guide', [
    'as' => 'guide',
    'uses' => 'home@guide'
]);

Route::get('/product', [
    'as' => 'product',
    'uses' => 'home@product'
]);





Auth::routes();

Route::get('/home', 'HomeController@index');
