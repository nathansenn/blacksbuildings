const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass([
        'main.scss'
    ], 'public/build/css/style.css');

    mix.styles([
        "build/css/style.css",
        "assets/css/animate.css",
        "assets/css/bootstrap.min.css",
        "assets/css/font-awesome.min.css",
        "assets/css/style.css",


    ], 'public/css/all.css', 'public/');





    mix.scripts([
        'assets/js/jquery.min.js',
        'assets/js/jquery-1.11.2.min.js',
        'assets/js/jquery.easing.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/filter.js',
        'assets/js/wow.js',
        'assets/js/custom.js',




    ], 'public/js/all.js', 'public/');



    mix.version(["public/css/all.css", "public/js/all.js"]);

    mix.copy("public/bower_components/bootstrap/dist/fonts/**", "public/build/fonts");

    mix.copy("public/assets/images/**", "public/build/images");

});

